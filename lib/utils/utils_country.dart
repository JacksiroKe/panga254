import 'package:flutter/material.dart';

import '../models/country.dart';
import 'constants/app_assets.dart';
import 'l10n.dart';

abstract class UtilsCountry {
  
  static String name(CountryType type, BuildContext context) {
    switch (type) {
      case CountryType.kenya:
        return context.l10n!.kenya;

      case CountryType.tanzania:
        return context.l10n!.tanzania;

      case CountryType.uganda:
        return context.l10n!.uganda;

    }
  }

  static List<String> facts(CountryType type, BuildContext context) {
    switch (type) {
      case CountryType.kenya:
        return [
          context.l10n!.kenyaFact1,
          context.l10n!.kenyaFact2,
          context.l10n!.kenyaFact3
        ];

      case CountryType.tanzania:
        return [
          context.l10n!.tanzaniaFact1,
          context.l10n!.tanzaniaFact2,
          context.l10n!.tanzaniaFact3
        ];

      case CountryType.uganda:
        return [
          context.l10n!.ugandaFact1,
          context.l10n!.ugandaFact2,
          context.l10n!.ugandaFact3
        ];

    }
  }

  static String image(CountryType type) {
    switch (type) {
      case CountryType.kenya:
        return AppAssets.kenyaImage;

      case CountryType.tanzania:
        return AppAssets.tanzaniaImage;

      case CountryType.uganda:
        return AppAssets.ugandaImage;

    }
  }

}
