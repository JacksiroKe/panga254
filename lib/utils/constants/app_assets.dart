abstract class AppAssets {
  static const themeMusic1 = 'audio/theme1.mp3';
  static const themeMusic2 = 'audio/theme2.mp3';
  static const themeMusic3 = 'audio/theme3.mp3';

  static const planetThemeMusic = 'audio/planet_theme_music.mp3';
  static const buttonClick = 'audio/button_click.mp3';
  static const visibility = 'audio/visibility.mp3';
  static const countDownBegin = 'audio/count_down_begin.mp3';
  static const completion = 'audio/completion.mp3';
  static const tileTapSuccess = 'audio/tile_tap_success.mp3';
  static const tileTapError = 'audio/tile_tap_error.mp3';

  static const appIcon = 'assets/icons/app_icon.png';

  static const imageBase = 'assets/images';

  static const nairobiImg = '$imageBase/nairobi.jpg';

  static const planetsBg = '$imageBase/planets.jpg';
  static const mercuryImage = '$imageBase/planets/mercury.png';
  static const venusImage = '$imageBase/planets/venus.png';
  static const earthImage = '$imageBase/planets/earth.png';
  static const marsImage = '$imageBase/planets/mars.png';
  static const jupiterImage = '$imageBase/planets/jupiter.png';
  static const saturnImage = '$imageBase/planets/saturn.png';
  static const uranusImage = '$imageBase/planets/uranus.png';
  static const neptuneImage = '$imageBase/planets/neptune.png';
  static const plutoImage = '$imageBase/planets/pluto.png';

  static const countriesBg = '$imageBase/countries.png';
  static const kenyaImage = '$imageBase/countries/kenya.png';
  static const tanzaniaImage = '$imageBase/countries/tanzania.png';
  static const ugandaImage = '$imageBase/countries/uganda.png';
  
}
