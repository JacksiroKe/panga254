import 'package:flutter/material.dart';

import '../../models/country.dart';
import '../../models/planet.dart';
import '../../models/puzzle.dart';
import '../../theme/themes/country_theme.dart';
import '../../theme/themes/planet_theme.dart';
import '../../theme/themes/puzzle_theme.dart';

// theme
const appTitle = 'Panga 254';
const kFontFamily = 'ComicSans';

// puzzle solving durations expected for perfect score
const k3PuzzleDuration = Duration(minutes: 2);
const k4PuzzleDuration = Duration(minutes: 5);
const k5PuzzleDuration = Duration(minutes: 8);

// durations
const kMS50 = Duration(milliseconds: 50);
const kMS100 = Duration(milliseconds: 100);
const kMS150 = Duration(milliseconds: 150);
const kMS200 = Duration(milliseconds: 200);
const kMS250 = Duration(milliseconds: 250);
const kMS300 = Duration(milliseconds: 300);
const kMS350 = Duration(milliseconds: 350);
const kMS400 = Duration(milliseconds: 400);
const kMS500 = Duration(milliseconds: 500);
const kMS800 = Duration(milliseconds: 800);
const kS1 = Duration(seconds: 1);
const kS4 = Duration(seconds: 4);
const kS20 = Duration(seconds: 20);

// fractional offset
const kFOTopLeft = FractionalOffset(0.05, 0.05);
const kFOTopRight = FractionalOffset(0.95, 0.05);
const kFOTopCenter = FractionalOffset(0.5, 0.05);
const kFOBottomLeft = FractionalOffset(0.05, 0.98);
const kFOBottomRight = FractionalOffset(0.95, 0.98);
const kFOBottomCenter = FractionalOffset(0.50, 0.98);

// background
const kBaseStarSize = 10.0;
const kMinStarPercentage = 0.20;
const kNoStars = 40;
const kBackgroundGradient = [
  Color(0xff0a0826),
  Color(0xff251f45),
  Color(0xff242021),
  Color(0xff251f58),
];

const kTotalPlanets = 9;

const Map<PuzzleLevel, int> kPuzzleLevel = {
  PuzzleLevel.easy: 3,
  PuzzleLevel.medium: 4,
  PuzzleLevel.hard: 5,
};

const Map<PuzzleKind, int> kPuzzleKind = {
  PuzzleKind.planets: 1,
};

const List<Planet> planets = [
  Planet(key: 1, type: PlanetType.mercury),
  Planet(key: 2, type: PlanetType.venus),
  Planet(key: 3, type: PlanetType.earth),
  Planet(key: 4, type: PlanetType.mars),
  Planet(key: 5, type: PlanetType.jupiter),
  Planet(key: 6, type: PlanetType.saturn),
  Planet(key: 7, type: PlanetType.uranus),
  Planet(key: 8, type: PlanetType.neptune),
];

const List<Country> countries = [
  Country(key: 1, type: CountryType.kenya),
  Country(key: 2, type: CountryType.tanzania),
  Country(key: 3, type: CountryType.uganda),
];

const Map<PlanetType, PuzzleTheme> planetThemeMap = {
  PlanetType.mercury: MercuryTheme(),
  PlanetType.venus: VenusTheme(),
  PlanetType.earth: EarthTheme(),
  PlanetType.mars: MarsTheme(),
  PlanetType.jupiter: JupiterTheme(),
  PlanetType.saturn: SaturnTheme(),
  PlanetType.uranus: UranusTheme(),
  PlanetType.neptune: NeptuneTheme(),
  PlanetType.pluto: PlutoTheme(),
};

const Map<CountryType, PuzzleTheme> countryThemeMap = {
  CountryType.kenya: KenyaTheme(),
  CountryType.tanzania: TanzaniaTheme(),
  CountryType.uganda: UgandaTheme(),
};

const kMinSunSize = 350.0;

const kGreyscaleColorFilter = ColorFilter.matrix(
  <double>[
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0.2126,
    0.7152,
    0.0722,
    0,
    0,
    0,
    0,
    0,
    1,
    0,
  ],
);
