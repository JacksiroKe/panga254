import 'package:equatable/equatable.dart';

enum CountryType { kenya, tanzania, uganda }

extension CountryTypeParsing on CountryType {
  String get value => name.split('.').last.toUpperCase();
}

class Country extends Equatable {
  final CountryType type;
  final int key;

  const Country({
    required this.key,
    required this.type,
  });

  String get name => type.name;

  @override
  List<Object?> get props => [type, key];

  @override
  String toString() {
    return type.toString().toUpperCase();
  }
}
