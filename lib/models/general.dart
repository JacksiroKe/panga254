class Selectable<T> {
  bool isSelected = false;
  T data;
  Selectable(this.data, this.isSelected);
}

class PlayingInfo {
  int id;
  PlayingInfo(this.id);
}
