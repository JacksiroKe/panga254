import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/audio/cubit/audio_player_cubit.dart';
import '../blocs/menu/bloc/menu_bloc.dart';
import '../blocs/menu/cubit/game_selection_cubit.dart';
import '../utils/constants/app_breakpoints.dart';
import '../widgets/menu/menu_carousel.dart';

class MenuSelection extends StatefulWidget {
  final Widget childWidget;

  const MenuSelection({Key? key, required this.childWidget}) : super(key: key);

  @override
  State<MenuSelection> createState() => MenuSelectionState();
}

class MenuSelectionState extends State<MenuSelection> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: AppBreakpoints.medium,
      child: widget.childWidget,
    );
  }
}

class MenuScreenSmall extends StatelessWidget {
  final Widget child;

  const MenuScreenSmall({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MenuSelection(childWidget: child);
  }
}

class MenuScreenMedium extends StatelessWidget {
  final Widget child;

  const MenuScreenMedium({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MenuSelection(childWidget: child);
  }
}

class MenuScreenLarge extends StatelessWidget {
  final MenuReady state;

  const MenuScreenLarge({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: LayoutBuilder(
        builder: (ctx, dimens) {
          return Center(
            child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                initialPage: 0,
                autoPlay: true,
              ),
              items: state.games.map((game) {
                return Builder(
                  builder: (BuildContext ctx) {
                    return MenuCarousel(
                      game: game,
                      height: dimens.maxHeight,
                      onPressed: () {
                        context.read<AudioPlayerCubit>().buttonClickAudio();
                        context.read<GameSelectionCubit>().onGameSelected(game);
                      },
                    );
                  },
                );
              }).toList(),
            ),
          );
        },
      ),
    );
  }
}
