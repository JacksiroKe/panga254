import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panga254/theme/themes/country_theme.dart';

import '../blocs/audio/cubit/audio_player_cubit.dart';
import '../blocs/playing/bloc/playing_bloc.dart';
import '../blocs/playing/cubit/fact_cubit.dart';
import '../blocs/menu/cubit/game_selection_cubit.dart';
import '../blocs/menu/cubit/level_selection_cubit.dart';
import '../blocs/puzzle/bloc/puzzle_bloc.dart';
import '../blocs/puzzle/cubit/puzzle_helper_cubit.dart';
import '../blocs/puzzle/cubit/puzzle_init_cubit.dart';
import '../blocs/timer/timer_bloc.dart';
import '../models/game.dart';
import '../models/puzzle.dart';
import '../models/ticker.dart';
import '../theme/bloc/theme_bloc.dart';
import '../theme/themes/puzzle_theme.dart';
import '../utils/constants/app_constants.dart';
import '../utils/utils.dart';
import '../utils/utils_country.dart';
import '../utils/utils_planet.dart';
import '../widgets/puzzle/puzzle_header.dart';
import '../widgets/puzzle/puzzle_sections.dart';

class PuzzleScreen extends StatelessWidget {
  const PuzzleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final game = context.read<GameSelectionCubit>().game;
    final planet = context.read<GameSelectionCubit>().planet;
    final country = context.read<GameSelectionCubit>().country;
    PuzzleTheme theme = const KenyaTheme();
    List<String> facts = [];

    if (game.type == GameType.coutries) {
      theme = countryThemeMap[country.type]!;
      facts = UtilsCountry.facts(country.type, context);
    } else {
      theme = planetThemeMap[planet.type]!;
      facts = UtilsPlanet.facts(planet.type, context);
    }
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => GameBloc(
            secondsToBegin: context.read<LevelSelectionCubit>().puzzleSize,
            ticker: const Ticker(),
          ),
        ),
        BlocProvider(
          create: (context) => PuzzleInitCubit(
            context.read<LevelSelectionCubit>().puzzleSize,
            context.read<GameBloc>(),
          ),
        ),
        BlocProvider(
          create: (context) => PuzzleBloc(
            context.read<LevelSelectionCubit>().puzzleSize,
            context.read<AudioPlayerCubit>(),
          )..add(const PuzzleInitialized(shufflePuzzle: false)),
        ),
        BlocProvider(
          create: (context) => PuzzleHelperCubit(
            context.read<PuzzleBloc>(),
            context.read<AudioPlayerCubit>(),
            optimized: Utils.isOptimizedPuzzle() ||
                context.read<LevelSelectionCubit>().puzzleLevel ==
                    PuzzleLevel.hard,
          ),
        ),
        BlocProvider(
          create: (_) => ThemeBloc(theme: theme),
        ),
        BlocProvider(
          create: (_) => TimerBloc(ticker: const Ticker()),
        ),
        BlocProvider(
          create: (_) => FactCubit(facts: facts),
        ),
      ],
      child: const PuzzleView(),
    );
  }
}

class PuzzleView extends StatelessWidget {
  const PuzzleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final theme = context.select((ThemeBloc bloc) => bloc.state.theme);
    // final state = context.select((PuzzleBloc bloc) => bloc.state);

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Utils.darkenColor(Colors.white),
          image: DecorationImage(
            image: AssetImage(context.read<GameSelectionCubit>().game.image),
            fit: BoxFit.cover,
          ),
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Stack(
              children: [
                SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: constraints.maxHeight,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        PuzzleHeader(),
                        PuzzleSections(),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
