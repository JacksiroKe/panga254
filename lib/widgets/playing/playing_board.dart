import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/audio/cubit/audio_player_cubit.dart';
import '../../blocs/playing/bloc/playing_bloc.dart';
import '../../blocs/menu/cubit/game_selection_cubit.dart';
import '../../blocs/menu/cubit/level_selection_cubit.dart';
import '../../blocs/puzzle/bloc/puzzle_bloc.dart';
import '../../blocs/puzzle/cubit/puzzle_helper_cubit.dart';
import '../../blocs/timer/timer_bloc.dart';
import '../../core/delegates/puzzle_layout_delegate.dart';
import '../../core/layout/responsive_layout_builder.dart';
import '../../utils/app_logger.dart';
import '../../utils/constants/app_constants.dart';
import '../../utils/helpers/modal_helpers.dart';
import 'completion_dialog.dart';

class PlayingBoard extends StatefulWidget {
  final List<Widget> tiles;

  const PlayingBoard({Key? key, required this.tiles}) : super(key: key);

  @override
  State<PlayingBoard> createState() => PlayingBoardState();
}

class PlayingBoardState extends State<PlayingBoard> {
  Timer? _completePuzzleTimer;

  void _onPuzzleCompletionDialog(BuildContext context) async {
    AppLogger.log('PlayingBoard: _onPuzzleCompletionDialog');

    // play completion audio
    context.read<AudioPlayerCubit>().completion();

    Timer(kMS300, () {
      // after dialog finishes, reset the puzzle to initial state
      context.read<GameBloc>().add(const ResetEvent());
    });

    // show dialog
    showAppDialog(
      context: context,

      /// for medium and large screen, same size
      sameSize: true,
      child: MultiBlocProvider(
        providers: [
          BlocProvider.value(value: context.read<LevelSelectionCubit>()),
          BlocProvider.value(value: context.read<GameSelectionCubit>()),
          BlocProvider.value(value: context.read<PuzzleHelperCubit>()),
          BlocProvider.value(value: context.read<TimerBloc>()),
          BlocProvider.value(value: context.read<PuzzleBloc>()),
        ],
        child: CompletionDialog(),
      ),
    );
  }

  @override
  void dispose() {
    _completePuzzleTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PuzzleBloc, PuzzleState>(
      listener: (BuildContext context, PuzzleState state) {
        if (state.puzzleStatus == PuzzleStatus.complete) {
          _completePuzzleTimer = Timer(kMS500, () {
            _onPuzzleCompletionDialog(context);
          });
        }
      },
      child: ResponsiveLayoutBuilder(
        small: (_, Widget? child) => PuzzleBoard(
          child: child,
          size: BoardSize.small,
        ),
        medium: (_, Widget? child) => PuzzleBoard(
          child: child,
          size: BoardSize.medium,
        ),
        large: (_, Widget? child) => PuzzleBoard(
          child: child,
          size: BoardSize.large,
        ),
        child: (_) => Stack(children: widget.tiles),
      ),
    );
  }
}

class PuzzleBoard extends StatelessWidget {
  final double size;
  final Widget? child;

  const PuzzleBoard({
    Key? key,
    this.child,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(dimension: size, child: child);
  }
}
