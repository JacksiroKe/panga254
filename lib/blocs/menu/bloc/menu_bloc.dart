import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../models/game.dart';
import '../../../models/planet.dart';
import '../../../utils/constants/app_assets.dart';
import '../../../utils/constants/app_breakpoints.dart';

part 'menu_event.dart';
part 'menu_state.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  List<Game> games = [];

  MenuBloc() : super(MenuLoading()) {
    on<MenuInitialized>(onMenuInit);
    on<MenuResized>(onMenuResized);
  }

  PlanetType getPlanetTypeAt(int index) {
    return PlanetType.values[index];
  }

  void generateGames() {
    games.add(
      const Game(
        key: 1,
        type: GameType.planets,
        title: 'Sayari',
        image: AppAssets.planetsBg,
      ),
    );
    games.add(
      const Game(
        key: 1,
        type: GameType.coutries,
        title: 'Nchi',
        image: AppAssets.countriesBg,
      ),
    );
  }

  void onMenuResized(
    MenuResized event,
    Emitter<MenuState> emit,
  ) {
    emit(MenuReady(games));
  }

  void onMenuInit(
    MenuInitialized event,
    Emitter<MenuState> emit,
  ) {
    Size size = event.size;

    if (event.size.width < AppBreakpoints.medium) {
      size = Size(AppBreakpoints.medium, event.size.height);
    }

    generateGames();

    emit(MenuReady(games));
  }
}
