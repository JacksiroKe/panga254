import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/country.dart';
import '../../../models/game.dart';
import '../../../models/planet.dart';
import '../../../utils/app_logger.dart';
import '../../../utils/constants/app_constants.dart';
import '../../../utils/utils.dart';
import '../../../views/puzzle_screen.dart';
import 'kind_selection_cubit.dart';
import 'level_selection_cubit.dart';

part 'game_selection_state.dart';

class GameSelectionCubit extends Cubit<GameSelectionState> {
  final LevelSelectionCubit levelSelectionCubit;
  final KindSelectionCubit kindSelectionCubit;
  final BuildContext context;

  GameSelectionCubit(
      this.levelSelectionCubit, this.kindSelectionCubit, this.context)
      : super(NoGameSelected());

  late Game _game;
  late Planet _planet;
  late Country _country;

  Game get game => _game;
  Planet get planet => _planet;
  Country get country => _country;

  void onGameSelected(Game game) async {
    _game = game;
    _planet = planets[0];
    _country = countries[0];
    
    switch (game.type) {
      case GameType.planets:
        var planetNos = [0, 1, 2, 3, 4, 5, 7, 8];
        planetNos.shuffle();
        _planet = planets[planetNos[0]];
        break;
      case GameType.coutries:
        var countryNos = [0, 1, 2];
        countryNos.shuffle();
        _country = countries[countryNos[0]];
        break;
    }

    AppLogger.log(
      'GameSelectionCubit tapped: $game: kind: ${kindSelectionCubit.state.kind} level: ${levelSelectionCubit.state.level}',
    );

    final page = await Utils.buildPageAsync(
      MultiBlocProvider(
        providers: [
          BlocProvider.value(value: kindSelectionCubit),
          BlocProvider.value(value: levelSelectionCubit),
          BlocProvider.value(value: this),
        ],
        child: const PuzzleScreen(key: Key('puzzle-page')),
      ),
    );

    await Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => page,
        transitionsBuilder: (_, anim, __, child) => FadeTransition(
          opacity: anim,
          child: child,
        ),
        transitionDuration: kMS800,
      ),
    );
  }
}
