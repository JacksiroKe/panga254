import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../models/puzzle.dart';
import '../../../utils/constants/app_constants.dart';

part 'kind_selection_state.dart';

class KindSelectionCubit extends Cubit<KindSelectionState> {
  KindSelectionCubit() : super(const KindSelectionState(PuzzleKind.planets));

  PuzzleKind _kind = PuzzleKind.planets;
  final _puzzleKinds = PuzzleKind.values;

  void onNewKindSelected(PuzzleKind newKind) {
    _kind = newKind;
    emit(KindSelectionState(newKind));
  }

  void onKindIncrease() {
    int currentKind = _puzzleKinds.indexOf(state.kind);
    currentKind = (currentKind + 1) % _puzzleKinds.length;
    onNewKindSelected(_puzzleKinds[currentKind]);
  }

  void onKindDecrease() {
    int currentKind = _puzzleKinds.indexOf(state.kind);
    currentKind = (currentKind - 1) % _puzzleKinds.length;
    onNewKindSelected(_puzzleKinds[currentKind]);
  }

  int get puzzleSize => kPuzzleKind[_kind]!;
  PuzzleKind get puzzleKind => _kind;
}
