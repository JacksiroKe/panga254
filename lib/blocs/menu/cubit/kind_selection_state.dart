part of 'kind_selection_cubit.dart';

class KindSelectionState extends Equatable {
  final PuzzleKind kind;

  const KindSelectionState(this.kind);

  @override
  List<Object> get props => [kind];
}
