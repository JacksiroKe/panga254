import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/menu/cubit/game_selection_cubit.dart';
import '../../models/country.dart';
import '../../models/game.dart';
import '../../models/planet.dart';
import '../themes/country_theme.dart';
import '../themes/planet_theme.dart';
import '../themes/puzzle_theme.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  static PuzzleTheme getTheme(PuzzleTheme theme) {
    return theme;
  }

  ThemeBloc({required PuzzleTheme theme})
      : super(ThemeState(theme: getTheme(theme))) {
    on<ThemeChangedEvent>(onThemeChanged);
  }

  void onThemeChanged(ThemeChangedEvent event, Emitter<ThemeState> emit) {
    emit(state.copyWith(theme: getTheme(event.theme)));
  }
}
