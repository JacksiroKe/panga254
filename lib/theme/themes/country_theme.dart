import '../../core/delegates/puzzle_layout_delegate.dart';
import '../../core/delegates/puzzle_layout.dart';
import '../../utils/constants/app_assets.dart';
import 'puzzle_theme.dart';

abstract class CountryTheme extends PuzzleTheme {
  const CountryTheme();

  @override
  String get name => 'Country';

  @override
  PuzzleLayout get puzzleLayoutDelegate => PuzzleLayoutDelegate();
}

class KenyaTheme extends CountryTheme {
  const KenyaTheme();

  @override
  String get assetForTile => AppAssets.kenyaImage;
}

class TanzaniaTheme extends CountryTheme {
  const TanzaniaTheme();

  @override
  String get assetForTile => AppAssets.tanzaniaImage;
}

class UgandaTheme extends CountryTheme {
  const UgandaTheme();

  @override
  String get assetForTile => AppAssets.ugandaImage;
}
